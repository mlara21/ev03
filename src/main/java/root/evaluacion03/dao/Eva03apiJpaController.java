/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion03.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.evaluacion03.dao.exceptions.NonexistentEntityException;
import root.evaluacion03.dao.exceptions.PreexistingEntityException;
import root.evaluacion03.entity.Eva03api;

/**
 *
 * @author mlara
 */
public class Eva03apiJpaController implements Serializable {

    public Eva03apiJpaController() {
       
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Eva03api eva03api) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(eva03api);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEva03api(eva03api.getRut()) != null) {
                throw new PreexistingEntityException("Eva03api " + eva03api + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Eva03api eva03api) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            eva03api = em.merge(eva03api);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = eva03api.getRut();
                if (findEva03api(id) == null) {
                    throw new NonexistentEntityException("The eva03api with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Eva03api eva03api;
            try {
                eva03api = em.getReference(Eva03api.class, id);
                eva03api.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The eva03api with id " + id + " no longer exists.", enfe);
            }
            em.remove(eva03api);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Eva03api> findEva03apiEntities() {
        return findEva03apiEntities(true, -1, -1);
    }

    public List<Eva03api> findEva03apiEntities(int maxResults, int firstResult) {
        return findEva03apiEntities(false, maxResults, firstResult);
    }

    private List<Eva03api> findEva03apiEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Eva03api.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Eva03api findEva03api(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Eva03api.class, id);
        } finally {
            em.close();
        }
    }

    public int getEva03apiCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Eva03api> rt = cq.from(Eva03api.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
