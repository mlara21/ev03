/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion03;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evaluacion03.dao.Eva03apiJpaController;
import root.evaluacion03.dao.exceptions.NonexistentEntityException;
import root.evaluacion03.entity.Eva03api;

/**
 *
 * @author mlara
 */
@Path("taller")
public class TallerMecanico {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarClientes(){
      
        Eva03apiJpaController dao=new Eva03apiJpaController();
        
        List<Eva03api> lista = dao.findEva03apiEntities();
        
        return Response.ok(200).entity(lista).build();
        
    
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Eva03api eva03api ){
    
        try {
            Eva03apiJpaController dao=new Eva03apiJpaController();
            dao.create(eva03api);
        } catch (Exception ex) {
            Logger.getLogger(TallerMecanico.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        return Response.ok(200).entity(eva03api).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete){
    
        try {
            Eva03apiJpaController dao=new Eva03apiJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(TallerMecanico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("cliente eliminado").build();
        
     
    }
    
    @PUT
    public Response update(Eva03api eva03api){
        
        try {
            Eva03apiJpaController dao=new Eva03apiJpaController();
            dao.edit(eva03api);
        } catch (Exception ex) {
            Logger.getLogger(TallerMecanico.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        return Response.ok(200).entity(eva03api).build();
    
    }
    
    

}
    
    
    
    

