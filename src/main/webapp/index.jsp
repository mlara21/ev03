<%-- 
    Document   : index
    Created on : 01-05-2021, 9:30:45
    Author     : mlara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <style>
            table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            }
        </style>
    </head>
    
    <h1>Miguel Lara</h1>
    <h2>Sección 50</h2>

    <table>
        <tr>
            <th>URL</th>
            <th>Link</th>

        </tr>
        <tr>
            <td>GET</td>
            <td>https://evaluacion03bbdd.herokuapp.com/api/taller</td>

        </tr>
        <tr>
            <td>POST</td>
            <td>https://evaluacion03bbdd.herokuapp.com/api/taller</td>

        </tr>
        <tr>
            <td>DELETE</td>
            <td>https://evaluacion03bbdd.herokuapp.com/api/taller/{id}</td>

        </tr>
        <tr>
            <td>PUT</td>
            <td>https://evaluacion03bbdd.herokuapp.com/api/taller</td>
        </tr>

    </table>

</html>
